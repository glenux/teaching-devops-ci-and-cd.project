FROM python:3.9

COPY . /app

WORKDIR /app

RUN pip install pipenv \
 && pipenv install \
 && chmod +x main.py

CMD pipenv run ./main.py
